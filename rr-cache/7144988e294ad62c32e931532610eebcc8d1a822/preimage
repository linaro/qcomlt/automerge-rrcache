// SPDX-License-Identifier: GPL-2.0
/*
 * SDM845 SoC device tree source
 *
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 */

#include <dt-bindings/interrupt-controller/arm-gic.h>
#include <dt-bindings/reset/qcom,sdm845-aoss.h>

/ {
	interrupt-parent = <&intc>;

	#address-cells = <2>;
	#size-cells = <2>;

	chosen { };

	memory@80000000 {
		device_type = "memory";
		/* We expect the bootloader to fill in the size */
		reg = <0 0x80000000 0 0>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		memory@85fc0000 {
			reg = <0 0x85fc0000 0 0x20000>;
			no-map;
		};

		memory@85fe0000 {
			compatible = "qcom,cmd-db";
			reg = <0x0 0x85fe0000 0x0 0x20000>;
			no-map;
		};

		smem_mem: memory@86000000 {
			reg = <0x0 0x86000000 0x0 0x200000>;
			no-map;
		};

		memory@86200000 {
			reg = <0 0x86200000 0 0x2d00000>;
			no-map;
		};

<<<<<<<
		adsp_mem: memory@8b100000 {
			reg = <0 0x8b100000 0 0x1a00000>;
			no-map;
		};

		cdsp_mem: memory@94700000 {
			reg = <0 0x94700000 0 0x800000>;
			no-map;
		};
=======
		mpss_mem: memory@8cc00000 {
			reg = <0 0x8cc00000 0 0x7600000>;
			no-map;
		};

		mba_mem: memory@94f00000 {
			reg = <0 0x94f00000 0 0x200000>;
			no-map;
		};

		rmtfs {
			compatible = "qcom,rmtfs-mem";

			size = <0x0 0x200000>;
			alloc-ranges = <0x0 0xa0000000 0x0 0x2000000>;
			no-map;

			qcom,client-id = <1>;
			qcom,vmid = <15>;
		};
>>>>>>>
	};

	cpus {
		#address-cells = <2>;
		#size-cells = <0>;

		CPU0: cpu@0 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x0>;
			enable-method = "psci";
			next-level-cache = <&L2_0>;
			L2_0: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
				L3_0: l3-cache {
				      compatible = "cache";
				};
			};
		};

		CPU1: cpu@100 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x100>;
			enable-method = "psci";
			next-level-cache = <&L2_100>;
			L2_100: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU2: cpu@200 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x200>;
			enable-method = "psci";
			next-level-cache = <&L2_200>;
			L2_200: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU3: cpu@300 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x300>;
			enable-method = "psci";
			next-level-cache = <&L2_300>;
			L2_300: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU4: cpu@400 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x400>;
			enable-method = "psci";
			next-level-cache = <&L2_400>;
			L2_400: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU5: cpu@500 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x500>;
			enable-method = "psci";
			next-level-cache = <&L2_500>;
			L2_500: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU6: cpu@600 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x600>;
			enable-method = "psci";
			next-level-cache = <&L2_600>;
			L2_600: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};

		CPU7: cpu@700 {
			device_type = "cpu";
			compatible = "qcom,kryo385";
			reg = <0x0 0x700>;
			enable-method = "psci";
			next-level-cache = <&L2_700>;
			L2_700: l2-cache {
				compatible = "cache";
				next-level-cache = <&L3_0>;
			};
		};
	};

	timer {
		compatible = "arm,armv8-timer";
		interrupts = <GIC_PPI 1 IRQ_TYPE_LEVEL_LOW>,
			     <GIC_PPI 2 IRQ_TYPE_LEVEL_LOW>,
			     <GIC_PPI 3 IRQ_TYPE_LEVEL_LOW>,
			     <GIC_PPI 0 IRQ_TYPE_LEVEL_LOW>;
	};

	adsp_pil: adsp-pil {
		compatible = "qcom,sdm845-adsp-pas";

		interrupts-extended = <&intc GIC_SPI 162 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 0 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 1 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 2 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 3 IRQ_TYPE_EDGE_RISING>;
		interrupt-names = "wdog", "fatal", "ready",
				  "handover", "stop-ack";

		clocks = <&xo_board>;
		clock-names = "xo";

		memory-region = <&adsp_mem>;

		qcom,smem-states = <&adsp_smp2p_out 0>;
		qcom,smem-state-names = "stop";

		status = "disabled";

		glink-edge {
			interrupts = <GIC_SPI 156 IRQ_TYPE_EDGE_RISING>;
			label = "lpass";
			qcom,remote-pid = <2>;
			mboxes = <&apss_shared 8>;
		};
	};

	cdsp_pil: cdsp-pil {
		compatible = "qcom,sdm845-cdsp-pas";

		interrupts-extended = <&intc GIC_SPI 578 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 0 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 1 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 2 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 3 IRQ_TYPE_EDGE_RISING>;
		interrupt-names = "wdog", "fatal", "ready",
				  "handover", "stop-ack";

		clocks = <&xo_board>;
		clock-names = "xo";

		memory-region = <&cdsp_mem>;

		qcom,smem-states = <&cdsp_smp2p_out 0>;
		qcom,smem-state-names = "stop";

		status = "disabled";

		glink-edge {
			interrupts = <GIC_SPI 574 IRQ_TYPE_EDGE_RISING>;
			label = "turing";
			qcom,remote-pid = <5>;
			mboxes = <&apss_shared 4>;
		};
	};

	clocks {
		xo_board: xo-board {
			compatible = "fixed-clock";
			#clock-cells = <0>;
			clock-frequency = <38400000>;
			clock-output-names = "xo_board";
		};

		sleep_clk: sleep-clk {
			compatible = "fixed-clock";
			#clock-cells = <0>;
			clock-frequency = <32764>;
		};
	};

	tcsr_mutex: hwlock {
		compatible = "qcom,tcsr-mutex";
		syscon = <&tcsr_mutex_regs 0 0x1000>;
		#hwlock-cells = <1>;
	};

	smem {
		compatible = "qcom,smem";
		memory-region = <&smem_mem>;
		hwlocks = <&tcsr_mutex 3>;
	};

<<<<<<<
=======
	smp2p-cdsp {
		compatible = "qcom,smp2p";
		qcom,smem = <94>, <432>;

		interrupts = <GIC_SPI 576 IRQ_TYPE_EDGE_RISING>;

		mboxes = <&apss_shared 6>;

		qcom,local-pid = <0>;
		qcom,remote-pid = <5>;

		cdsp_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		cdsp_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";

			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

	smp2p-lpass {
		compatible = "qcom,smp2p";
		qcom,smem = <443>, <429>;

		interrupts = <GIC_SPI 158 IRQ_TYPE_EDGE_RISING>;

		mboxes = <&apss_shared 10>;

		qcom,local-pid = <0>;
		qcom,remote-pid = <2>;

		adsp_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		adsp_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";

			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

>>>>>>>
	smp2p-mpss {
		compatible = "qcom,smp2p";
		qcom,smem = <435>, <428>;
		interrupts = <GIC_SPI 451 IRQ_TYPE_EDGE_RISING>;
		mboxes = <&apss_shared 14>;
		qcom,local-pid = <0>;
		qcom,remote-pid = <1>;

		modem_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		modem_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";
			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

<<<<<<<
=======
	smp2p-slpi {
		compatible = "qcom,smp2p";
		qcom,smem = <481>, <430>;
		interrupts = <GIC_SPI 172 IRQ_TYPE_EDGE_RISING>;
		mboxes = <&apss_shared 26>;
		qcom,local-pid = <0>;
		qcom,remote-pid = <3>;

		slpi_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		slpi_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";
			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

>>>>>>>
	psci {
		compatible = "arm,psci-1.0";
		method = "smc";
	};

	soc: soc {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0 0 0 0xffffffff>;
		compatible = "simple-bus";

		gcc: clock-controller@100000 {
			compatible = "qcom,gcc-sdm845";
			reg = <0x100000 0x1f0000>;
			#clock-cells = <1>;
			#reset-cells = <1>;
			#power-domain-cells = <1>;
		};

		tcsr_mutex_regs: syscon@1f40000 {
			compatible = "syscon";
			reg = <0x1f40000 0x40000>;
		};

		tlmm: pinctrl@3400000 {
			compatible = "qcom,sdm845-pinctrl";
			reg = <0x03400000 0xc00000>;
			interrupts = <GIC_SPI 208 IRQ_TYPE_LEVEL_HIGH>;
			gpio-controller;
			#gpio-cells = <2>;
			interrupt-controller;
			#interrupt-cells = <2>;
		};

<<<<<<<
		aoss_reset: qcom,reset-controller@c2b0000 {
			compatible = "qcom,sdm845-aoss-reset";
			reg = <0xc2b0000 0x21000>;
			#reset-cells = <1>;
=======
		remoteproc@4080000 {
			compatible = "qcom,sdm845-mss-pil";
			reg = <0x04080000 0x408>, <0x04180000 0x48>;

			reg-names = "qdsp6", "rmb";

			interrupts-extended = <&intc 0 266 1>,
					      <&modem_smp2p_in 0 0>,
					      <&modem_smp2p_in 1 0>,
					      <&modem_smp2p_in 2 0>,
					      <&modem_smp2p_in 3 0>;

			interrupt-names = "wdog", "fatal", "ready",
					  "handover", "stop-ack";

			clocks = <&gcc GCC_MSS_CFG_AHB_CLK>,
				 <&gcc GCC_MSS_Q6_MEMNOC_AXI_CLK>,
				 <&gcc GCC_BOOT_ROM_AHB_CLK>,
				 <&gcc GCC_MSS_GPLL0_DIV_CLK_SRC>,
				 <&gcc GCC_MSS_SNOC_AXI_CLK>,
				 <&gcc GCC_MSS_AXIS2_CLK>,
				 <&gcc GCC_MSS_MFAB_AXIS_CLK>,
				 <&gcc GCC_PRNG_AHB_CLK>,
				 <&xo_board>;

			clock-names = "iface", "bus", "mem", "gpll0_mss",
				      "snoc_axi", "axis2", "mnoc_axi",
				      "prng", "xo";

			qcom,smem-states = <&modem_smp2p_out 0>;
			qcom,smem-state-names = "stop";

			resets = <&aoss_reset AOSS_CC_MSS_RESTART>;
			reset-names = "mss_restart";

			/* cx-supply = <&pm8998_s9>; */
			/* mx-supply = <&pm8998_s6>; */
			/* mss-supply = <&pm8005_s2_level>; */

			qcom,halt-regs = <&tcsr_mutex_regs 0x23000 0x25000 0x24000>;

			mba {
				memory-region = <&mba_mem>;
			};

			mpss {
				memory-region = <&mpss_mem>;
			};

			glink-edge {
				interrupts = <0 449 IRQ_TYPE_EDGE_RISING>;
				label = "modem";
				qcom,remote-pid = <1>;
				mboxes = <&apss_shared 12>;
				mbox-names = "mpss_smem";
			};
>>>>>>>
		};

		spmi_bus: spmi@c440000 {
			compatible = "qcom,spmi-pmic-arb";
			reg = <0xc440000 0x1100>,
			      <0xc600000 0x2000000>,
			      <0xe600000 0x100000>,
			      <0xe700000 0xa0000>,
			      <0xc40a000 0x26000>;
			reg-names = "core", "chnls", "obsrvr", "intr", "cnfg";
			interrupt-names = "periph_irq";
			interrupts = <GIC_SPI 481 IRQ_TYPE_LEVEL_HIGH>;
			qcom,ee = <0>;
			qcom,channel = <0>;
			#address-cells = <2>;
			#size-cells = <0>;
			interrupt-controller;
			#interrupt-cells = <4>;
			cell-index = <0>;
		};

		apss_shared: mailbox@17990000 {
			compatible = "qcom,sdm845-apss-shared";
			reg = <0x17990000 0x1000>;
			#mbox-cells = <1>;
		};

		intc: interrupt-controller@17a00000 {
			compatible = "arm,gic-v3";
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			#interrupt-cells = <3>;
			interrupt-controller;
			reg = <0x17a00000 0x10000>,     /* GICD */
			      <0x17a60000 0x100000>;    /* GICR * 8 */
			interrupts = <GIC_PPI 9 IRQ_TYPE_LEVEL_HIGH>;

			gic-its@17a40000 {
				compatible = "arm,gic-v3-its";
				msi-controller;
				#msi-cells = <1>;
				reg = <0x17a40000 0x20000>;
				status = "disabled";
			};
		};

		timer@17c90000 {
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			compatible = "arm,armv7-timer-mem";
			reg = <0x17c90000 0x1000>;

			frame@17ca0000 {
				frame-number = <0>;
				interrupts = <GIC_SPI 7 IRQ_TYPE_LEVEL_HIGH>,
					     <GIC_SPI 6 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17ca0000 0x1000>,
				      <0x17cb0000 0x1000>;
			};

			frame@17cc0000 {
				frame-number = <1>;
				interrupts = <GIC_SPI 8 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17cc0000 0x1000>;
				status = "disabled";
			};

			frame@17cd0000 {
				frame-number = <2>;
				interrupts = <GIC_SPI 9 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17cd0000 0x1000>;
				status = "disabled";
			};

			frame@17ce0000 {
				frame-number = <3>;
				interrupts = <GIC_SPI 10 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17ce0000 0x1000>;
				status = "disabled";
			};

			frame@17cf0000 {
				frame-number = <4>;
				interrupts = <GIC_SPI 11 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17cf0000 0x1000>;
				status = "disabled";
			};

			frame@17d00000 {
				frame-number = <5>;
				interrupts = <GIC_SPI 12 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17d00000 0x1000>;
				status = "disabled";
			};

			frame@17d10000 {
				frame-number = <6>;
				interrupts = <GIC_SPI 13 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x17d10000 0x1000>;
				status = "disabled";
			};
		};
	};
};
