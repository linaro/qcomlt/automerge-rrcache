// SPDX-License-Identifier: GPL-2.0
// Copyright (c) 2018, Linaro Limited

#include <dt-bindings/interrupt-controller/arm-gic.h>
#include <dt-bindings/clock/qcom,gcc-qcs404.h>
#include <dt-bindings/clock/qcom,rpmcc.h>

/ {
	interrupt-parent = <&intc>;

	#address-cells = <2>;
	#size-cells = <2>;

	chosen { };

	clocks {
		xo_board: xo-board {
			compatible = "fixed-clock";
			#clock-cells = <0>;
			clock-frequency = <19200000>;
		};
	};

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		CPU0: cpu@100 {
			device_type = "cpu";
			compatible = "arm,cortex-a53";
			reg = <0x100>;
			enable-method = "psci";
			next-level-cache = <&L2_0>;
<<<<<<<
			#cooling-cells= <2>;
=======
			clocks = <&apcs_glb>;
			operating-points-v2 = <&cpu_opp_table>;
>>>>>>>
		};

		CPU1: cpu@101 {
			device_type = "cpu";
			compatible = "arm,cortex-a53";
			reg = <0x101>;
			enable-method = "psci";
			next-level-cache = <&L2_0>;
<<<<<<<
			#cooling-cells= <2>;
=======
			clocks = <&apcs_glb>;
			operating-points-v2 = <&cpu_opp_table>;
>>>>>>>
		};

		CPU2: cpu@102 {
			device_type = "cpu";
			compatible = "arm,cortex-a53";
			reg = <0x102>;
			enable-method = "psci";
			next-level-cache = <&L2_0>;
<<<<<<<
			#cooling-cells= <2>;
=======
			clocks = <&apcs_glb>;
			operating-points-v2 = <&cpu_opp_table>;
>>>>>>>
		};

		CPU3: cpu@103 {
			device_type = "cpu";
			compatible = "arm,cortex-a53";
			reg = <0x103>;
			enable-method = "psci";
			next-level-cache = <&L2_0>;
<<<<<<<
			#cooling-cells= <2>;
=======
			clocks = <&apcs_glb>;
			operating-points-v2 = <&cpu_opp_table>;
>>>>>>>
		};

		L2_0: l2-cache {
			compatible = "cache";
			cache-level = <2>;
		};
	};

	cpu_opp_table: cpu_opp_table {
		compatible = "operating-points-v2";
		opp-shared;

		opp-1094400000 {
			opp-hz = /bits/ 64 <1094400000>;
		};
		opp-1248000000 {
			opp-hz = /bits/ 64 <1248000000>;
		};
		opp-1401600000 {
			opp-hz = /bits/ 64 <1401600000>;
		};
	};

	firmware {
		scm: scm {
			compatible = "qcom,scm-qcs404", "qcom,scm";
			#reset-cells = <1>;
		};
	};

	memory@80000000 {
		device_type = "memory";
		/* We expect the bootloader to fill in the size */
		reg = <0 0x80000000 0 0>;
	};

	psci {
		compatible = "arm,psci-1.0";
		method = "smc";
	};

	remoteproc_adsp: remoteproc-adsp {
		compatible = "qcom,qcs404-adsp-pas";

		interrupts-extended = <&intc GIC_SPI 293 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 0 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 1 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 2 IRQ_TYPE_EDGE_RISING>,
				      <&adsp_smp2p_in 3 IRQ_TYPE_EDGE_RISING>;
		interrupt-names = "wdog", "fatal", "ready",
				  "handover", "stop-ack";

		clocks = <&xo_board>;
		clock-names = "xo";

		memory-region = <&adsp_fw_mem>;

		qcom,smem-states = <&adsp_smp2p_out 0>;
		qcom,smem-state-names = "stop";

		status = "disabled";

		glink-edge {
			interrupts = <GIC_SPI 289 IRQ_TYPE_EDGE_RISING>;

			qcom,remote-pid = <2>;
			mboxes = <&apcs_glb 8>;

			label = "adsp";

			#address-cells = <1>;
			#size-cells = <0>;

			fastrpc {
				compatible = "qcom,fastrpc";
				qcom,glink-channels = "fastrpcglink-apps-dsp";
				reg = <0>;

				#address-cells = <1>;
				#size-cells = <0>;

				qcom,msm_fastrpc_compute_cb_1 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <4>;
				};

				qcom,msm_fastrpc_compute_cb_2 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <5>;
				};

				qcom,msm_fastrpc_compute_cb_3 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <6>;
					shared-cb = <5>;
				};
			};
		};
	};

	remoteproc_cdsp: remoteproc-cdsp {
		compatible = "qcom,qcs404-cdsp-pas";

		interrupts-extended = <&intc GIC_SPI 229 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 0 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 1 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 2 IRQ_TYPE_EDGE_RISING>,
				      <&cdsp_smp2p_in 3 IRQ_TYPE_EDGE_RISING>;
		interrupt-names = "wdog", "fatal", "ready",
				  "handover", "stop-ack";

		clocks = <&xo_board>;
		clock-names = "xo";

		memory-region = <&cdsp_fw_mem>;

		qcom,smem-states = <&cdsp_smp2p_out 0>;
		qcom,smem-state-names = "stop";

		status = "disabled";

		glink-edge {
			interrupts = <GIC_SPI 141 IRQ_TYPE_EDGE_RISING>;

			qcom,remote-pid = <5>;
			mboxes = <&apcs_glb 12>;

			label = "cdsp";

			#address-cells = <1>;
			#size-cells = <0>;

			fastrpc {
				compatible = "qcom,fastrpc";
				qcom,glink-channels = "fastrpcglink-apps-dsp";
				reg = <3>;

				#address-cells = <1>;
				#size-cells = <0>;

				qcom,msm_fastrpc_compute_cb_1 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <1>;
				};

				qcom,msm_fastrpc_compute_cb_2 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <2>;
				};

				qcom,msm_fastrpc_compute_cb_3 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <3>;
				};

				qcom,msm_fastrpc_compute_cb_4 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <4>;
				};

				qcom,msm_fastrpc_compute_cb_5 {
					compatible = "qcom,fastrpc-compute-cb";
					reg = <5>;
				};
			};
		};
	};

	remoteproc_wcss: remoteproc-wcss {
		compatible = "qcom,qcs404-wcss-pas";

		interrupts-extended = <&intc GIC_SPI 153 IRQ_TYPE_EDGE_RISING>,
				      <&wcss_smp2p_in 0 IRQ_TYPE_EDGE_RISING>,
				      <&wcss_smp2p_in 1 IRQ_TYPE_EDGE_RISING>,
				      <&wcss_smp2p_in 2 IRQ_TYPE_EDGE_RISING>,
				      <&wcss_smp2p_in 3 IRQ_TYPE_EDGE_RISING>;
		interrupt-names = "wdog", "fatal", "ready",
				  "handover", "stop-ack";

		clocks = <&xo_board>;
		clock-names = "xo";

		memory-region = <&wlan_fw_mem>;

		qcom,smem-states = <&wcss_smp2p_out 0>;
		qcom,smem-state-names = "stop";

		status = "disabled";

		glink-edge {
			interrupts = <GIC_SPI 156 IRQ_TYPE_EDGE_RISING>;

			qcom,remote-pid = <1>;
			mboxes = <&apcs_glb 16>;

			label = "wcss";
		};
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		memory@85600000 {
			reg = <0 0x85600000 0 0x90000>;
			no-map;
		};

		smem_region: memory@85f00000 {
			reg = <0 0x85f00000 0 0x200000>;
			no-map;
		};

		memory@86100000 {
			reg = <0 0x86100000 0 0x300000>;
			no-map;
		};

		wlan_fw_mem: memory@86400000 {
			reg = <0 0x86400000 0 0x1c00000>;
			no-map;
		};

		adsp_fw_mem: memory@88000000 {
			reg = <0 0x88000000 0 0x1a00000>;
			no-map;
		};

		cdsp_fw_mem: memory@89a00000 {
			reg = <0 0x89a00000 0 0x600000>;
			no-map;
		};

		wlan_msa_mem: memory@8a000000 {
			reg = <0 0x8a000000 0 0x100000>;
			no-map;
		};
	};

	rpm-glink {
		compatible = "qcom,glink-rpm";

		interrupts = <GIC_SPI 168 IRQ_TYPE_EDGE_RISING>;
		qcom,rpm-msg-ram = <&rpm_msg_ram>;
		mboxes = <&apcs_glb 0>;

		rpm_requests: glink-channel {
			compatible = "qcom,rpm-qcs404";
			qcom,glink-channels = "rpm_requests";

			rpmcc: clock-controller {
				compatible = "qcom,rpmcc-qcs404";
				#clock-cells = <1>;
			};
		};
	};

	smem {
		compatible = "qcom,smem";

		memory-region = <&smem_region>;
		qcom,rpm-msg-ram = <&rpm_msg_ram>;

		hwlocks = <&tcsr_mutex 3>;
	};

	tcsr_mutex: hwlock {
		compatible = "qcom,tcsr-mutex";
		syscon = <&tcsr_mutex_regs 0 0x1000>;
		#hwlock-cells = <1>;
	};

	soc: soc@0 {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges = <0 0 0 0xffffffff>;
		compatible = "simple-bus";

		rpm_msg_ram: memory@60000 {
			compatible = "qcom,rpm-msg-ram";
			reg = <0x00060000 0x6000>;
		};

		qfprom: qfprom@a4000 {
			compatible = "qcom,qfprom";
			reg = <0x000a4000 0x1000>;
			#address-cells = <1>;
			#size-cells = <1>;
			tsens_caldata: caldata@d0 {
				reg = <0x1f8 0x14>;
			};
		};

		rng: rng@e3000 {
			compatible = "qcom,prng-ee";
			reg = <0x000e3000 0x1000>;
			clocks = <&gcc GCC_PRNG_AHB_CLK>;
			clock-names = "core";
		};

<<<<<<<
		tsens: thermal-sensor@4a9000 {
			compatible = "qcom,qcs404-tsens", "qcom,tsens-v1";
			reg = <0x004a9000 0x1000>, /* TM */
			      <0x004a8000 0x1000>; /* SROT */
			nvmem-cells = <&tsens_caldata>;
			nvmem-cell-names = "calib";
			#qcom,sensors = <10>;
			#thermal-sensor-cells = <1>;
=======
		usb3_phy: phy@78000 {
			compatible = "qcom,usb-ssphy";
			reg = <0x78000 0x400>;
			#phy-cells = <0>;
			clocks = <&rpmcc RPM_SMD_LN_BB_CLK>,
				 <&gcc GCC_USB_HS_PHY_CFG_AHB_CLK>,
				 <&gcc GCC_USB3_PHY_PIPE_CLK>;
			clock-names = "ref", "phy", "pipe";
			resets = <&gcc GCC_USB3_PHY_BCR>,
				 <&gcc GCC_USB3PHY_PHY_BCR>;
			reset-names = "com", "phy";
			status = "disabled";
		};

		usb2_phy_prim: phy@7a000 {
			compatible = "qcom,qcs404-usb-hsphy";
			reg = <0x7a000 0x200>;
			#phy-cells = <0>;
			clocks = <&rpmcc RPM_SMD_LN_BB_CLK>,
				 <&gcc GCC_USB_HS_PHY_CFG_AHB_CLK>,
				 <&gcc GCC_USB2A_PHY_SLEEP_CLK>;
			clock-names = "ref", "phy", "sleep";
			resets = <&gcc GCC_USB_HS_PHY_CFG_AHB_BCR>,
				 <&gcc GCC_USB2A_PHY_BCR>;
			reset-names = "phy", "por";
			qcom,init-seq = <0xc0 0x01 0>,
					<0xe8 0x0d 0>,
					<0x74 0x12 0>,
					<0x98 0x63 0>,
					<0x9c 0x03 0>,
					<0xa0 0x1d 0>,
					<0xa4 0x03 0>,
					<0x8c 0x23 0>,
					<0x78 0x08 0>,
					<0x7c 0xdc 0>,
					<0x90 0xe0 20>,
					<0x74 0x10 0>,
					<0x90 0x60 0>;
			status = "disabled";
		};

		usb2_phy_sec: phy@7c000 {
			compatible = "qcom,qcs404-usb-hsphy";
			reg = <0x7c000 0x200>;
			#phy-cells = <0>;
			clocks = <&rpmcc RPM_SMD_LN_BB_CLK>,
				 <&gcc GCC_USB_HS_PHY_CFG_AHB_CLK>,
				 <&gcc GCC_USB2A_PHY_SLEEP_CLK>;
			clock-names = "ref", "phy", "sleep";
			resets = <&gcc GCC_QUSB2_PHY_BCR>,
				 <&gcc GCC_USB2_HS_PHY_ONLY_BCR>;
			reset-names = "phy", "por";
			qcom,init-seq = <0xc0 0x01 0>,
					<0xe8 0x0d 0>,
					<0x74 0x12 0>,
					<0x98 0x63 0>,
					<0x9c 0x03 0>,
					<0xa0 0x1d 0>,
					<0xa4 0x03 0>,
					<0x8c 0x23 0>,
					<0x78 0x08 0>,
					<0x7c 0xdc 0>,
					<0x90 0xe0 20>,
					<0x74 0x10 0>,
					<0x90 0x60 0>;
			status = "disabled";
		};

		usb3: usb@7678800 {
			compatible = "qcom,dwc3";
			reg = <0x07678800 0x400>;
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			clocks = <&gcc GCC_USB30_MASTER_CLK>,
				 <&gcc GCC_SYS_NOC_USB3_CLK>,
				 <&gcc GCC_USB30_SLEEP_CLK>,
				 <&gcc GCC_USB30_MOCK_UTMI_CLK>;
			clock-names = "core", "iface", "sleep", "mock_utmi";
			assigned-clocks = <&gcc GCC_USB20_MOCK_UTMI_CLK>,
					  <&gcc GCC_USB30_MASTER_CLK>;
			assigned-clock-rates = <19200000>, <200000000>;
			status = "disabled";

			dwc3@7580000 {
				compatible = "snps,dwc3";
				reg = <0x07580000 0xcd00>;
				interrupts = <GIC_SPI 26 IRQ_TYPE_LEVEL_HIGH>;
				phys = <&usb2_phy_sec>, <&usb3_phy>;
				phy-names = "usb2-phy", "usb3-phy";
				snps,has-lpm-erratum;
				snps,hird-threshold = /bits/ 8 <0x10>;
				snps,usb3_lpm_capable;
				dr_mode = "otg";
			};
		};

		usb2: usb@79b8800 {
			compatible = "qcom,dwc3";
			reg = <0x079b8800 0x400>;
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			clocks = <&gcc GCC_USB_HS_SYSTEM_CLK>,
				 <&gcc GCC_PCNOC_USB2_CLK>,
				 <&gcc GCC_USB_HS_INACTIVITY_TIMERS_CLK>,
				 <&gcc GCC_USB20_MOCK_UTMI_CLK>;
			clock-names = "core", "iface", "sleep", "mock_utmi";
			assigned-clocks = <&gcc GCC_USB20_MOCK_UTMI_CLK>,
					  <&gcc GCC_USB_HS_SYSTEM_CLK>;
			assigned-clock-rates = <19200000>, <133333333>;
			status = "disabled";

			dwc3@78c0000 {
				compatible = "snps,dwc3";
				reg = <0x078c0000 0xcc00>;
				interrupts = <GIC_SPI 44 IRQ_TYPE_LEVEL_HIGH>;
				phys = <&usb2_phy_prim>;
				phy-names = "usb2-phy";
				snps,has-lpm-erratum;
				snps,hird-threshold = /bits/ 8 <0x10>;
				snps,usb3_lpm_capable;
				dr_mode = "peripheral";
			};
>>>>>>>
		};

		tlmm: pinctrl@1000000 {
			compatible = "qcom,qcs404-pinctrl";
			reg = <0x01000000 0x200000>,
			      <0x01300000 0x200000>,
			      <0x07b00000 0x200000>;
			reg-names = "south", "north", "east";
			interrupts = <GIC_SPI 208 IRQ_TYPE_LEVEL_HIGH>;
			gpio-ranges = <&tlmm 0 0 120>;
			gpio-controller;
			#gpio-cells = <2>;
			interrupt-controller;
			#interrupt-cells = <2>;
		};

		gcc: clock-controller@1800000 {
			compatible = "qcom,gcc-qcs404";
			reg = <0x01800000 0x80000>;
			#clock-cells = <1>;
			#reset-cells = <1>;

			assigned-clocks = <&gcc GCC_APSS_AHB_CLK_SRC>;
			assigned-clock-rates = <19200000>;
		};

		tcsr_mutex_regs: syscon@1905000 {
			compatible = "syscon";
			reg = <0x01905000 0x20000>;
		};

		spmi_bus: spmi@200f000 {
			compatible = "qcom,spmi-pmic-arb";
			reg = <0x0200f000 0x001000>,
			      <0x02400000 0x800000>,
			      <0x02c00000 0x800000>,
			      <0x03800000 0x200000>,
			      <0x0200a000 0x002100>;
			reg-names = "core", "chnls", "obsrvr", "intr", "cnfg";
			interrupt-names = "periph_irq";
			interrupts = <GIC_SPI 190 IRQ_TYPE_LEVEL_HIGH>;
			qcom,ee = <0>;
			qcom,channel = <0>;
			#address-cells = <2>;
			#size-cells = <0>;
			interrupt-controller;
			#interrupt-cells = <4>;
		};

		sdcc1: sdcc@7804000 {
			compatible = "qcom,sdhci-msm-v5";
			reg = <0x07804000 0x1000>, <0x7805000 0x1000>;
			reg-names = "hc_mem", "cmdq_mem";

			interrupts = <GIC_SPI 123 IRQ_TYPE_LEVEL_HIGH>,
				     <GIC_SPI 138 IRQ_TYPE_LEVEL_HIGH>;
			interrupt-names = "hc_irq", "pwr_irq";

			clocks = <&gcc GCC_SDCC1_APPS_CLK>,
				 <&gcc GCC_SDCC1_AHB_CLK>,
				 <&xo_board>;
			clock-names = "core", "iface", "xo";

			status = "disabled";
		};

		blsp1_dma: dma@7884000 {
			compatible = "qcom,bam-v1.7.0";
			reg = <0x07884000 0x25000>;
			interrupts = <GIC_SPI 238 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&gcc GCC_BLSP1_AHB_CLK>;
			clock-names = "bam_clk";
			#dma-cells = <1>;
			qcom,controlled-remotely = <1>;
			qcom,ee = <0>;
			status = "okay";
		};

		blsp1_uart2: serial@78b1000 {
			compatible = "qcom,msm-uartdm-v1.4", "qcom,msm-uartdm";
			reg = <0x078b1000 0x200>;
			interrupts = <GIC_SPI 118 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&gcc GCC_BLSP1_UART2_APPS_CLK>, <&gcc GCC_BLSP1_AHB_CLK>;
			clock-names = "core", "iface";
			dmas = <&blsp1_dma 5>, <&blsp1_dma 4>;
			dma-names = "rx", "tx";
			status = "okay";
		};

		intc: interrupt-controller@b000000 {
			compatible = "qcom,msm-qgic2";
			interrupt-controller;
			#interrupt-cells = <3>;
			reg = <0x0b000000 0x1000>,
			      <0x0b002000 0x1000>;
		};

		apcs_glb: mailbox@b011000 {
			compatible = "qcom,qcs404-apcs-apps-global", "syscon";
			reg = <0x0b011000 0x1000>;
			#mbox-cells = <1>;
			clocks = <&gcc GCC_GPLL0_AO_OUT_MAIN>, <&apcs_hfpll>;
			clock-names = "aux", "pll";
			#clock-cells = <0>;
		};

		apcs_hfpll: clock-controller@0b016000 {
			compatible = "qcom,hfpll";
			reg = <0x0b016000 0x30>;
			#clock-cells = <0>;
			clock-output-names = "apcs_hfpll";
			clocks = <&xo_board>;
			clock-names = "xo";
		};

		timer@b120000 {
			#address-cells = <1>;
			#size-cells = <1>;
			ranges;
			compatible = "arm,armv7-timer-mem";
			reg = <0x0b120000 0x1000>;
			clock-frequency = <19200000>;

			frame@b121000 {
				frame-number = <0>;
				interrupts = <GIC_SPI 8 IRQ_TYPE_LEVEL_HIGH>,
					     <GIC_SPI 7 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b121000 0x1000>,
				      <0x0b122000 0x1000>;
			};

			frame@b123000 {
				frame-number = <1>;
				interrupts = <GIC_SPI 9 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b123000 0x1000>;
				status = "disabled";
			};

			frame@b124000 {
				frame-number = <2>;
				interrupts = <GIC_SPI 10 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b124000 0x1000>;
				status = "disabled";
			};

			frame@b125000 {
				frame-number = <3>;
				interrupts = <GIC_SPI 11 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b125000 0x1000>;
				status = "disabled";
			};

			frame@b126000 {
				frame-number = <4>;
				interrupts = <GIC_SPI 12 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b126000 0x1000>;
				status = "disabled";
			};

			frame@b127000 {
				frame-number = <5>;
				interrupts = <GIC_SPI 13 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0xb127000 0x1000>;
				status = "disabled";
			};

			frame@b128000 {
				frame-number = <6>;
				interrupts = <GIC_SPI 14 IRQ_TYPE_LEVEL_HIGH>;
				reg = <0x0b128000 0x1000>;
				status = "disabled";
			};
		};
	};

	timer {
		compatible = "arm,armv8-timer";
		interrupts = <GIC_PPI 2 0xff08>,
			     <GIC_PPI 3 0xff08>,
			     <GIC_PPI 4 0xff08>,
			     <GIC_PPI 1 0xff08>;
	};

	smp2p-adsp {
		compatible = "qcom,smp2p";
		qcom,smem = <443>, <429>;
		interrupts = <GIC_SPI 291 IRQ_TYPE_EDGE_RISING>;
		mboxes = <&apcs_glb 10>;
		qcom,local-pid = <0>;
		qcom,remote-pid = <2>;

		adsp_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		adsp_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";
			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

	smp2p-cdsp {
		compatible = "qcom,smp2p";
		qcom,smem = <94>, <432>;
		interrupts = <GIC_SPI 143 IRQ_TYPE_EDGE_RISING>;
		mboxes = <&apcs_glb 14>;
		qcom,local-pid = <0>;
		qcom,remote-pid = <5>;

		cdsp_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		cdsp_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";
			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

	smp2p-wcss {
		compatible = "qcom,smp2p";
		qcom,smem = <435>, <428>;
		interrupts = <GIC_SPI 158 IRQ_TYPE_EDGE_RISING>;
		mboxes = <&apcs_glb 18>;
		qcom,local-pid = <0>;
		qcom,remote-pid = <1>;

		wcss_smp2p_out: master-kernel {
			qcom,entry-name = "master-kernel";
			#qcom,smem-state-cells = <1>;
		};

		wcss_smp2p_in: slave-kernel {
			qcom,entry-name = "slave-kernel";
			interrupt-controller;
			#interrupt-cells = <2>;
		};
	};

	thermal-zones {
		aoss-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 0>;

			trips {
				aoss_alert: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				aoss_crit: trip1 {
					temperature = <95000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		dsp-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 1>;

			trips {
				dsp_alert: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				dsp_crit: trip1 {
					temperature = <95000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		lpass-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 2>;

			trips {
				lpass_alert: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				lpass_crit: trip1 {
					temperature = <95000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		wlan-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 3>;

			trips {
				wlan_alert: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				wlan_crit: trip1 {
					temperature = <95000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		cluster-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 4>;

			trips {
				cluster_alert: cluster_alert {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				cluster_crit: cluster_crit {
					temperature = <110000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		cpu-thermal0 {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 5>;

			trips {
				cpu_alert0: cpu_alert0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				cpu_crit0: cpu_crit0 {
					temperature = <110000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		cpu-thermal1 {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 6>;

			trips {
				cpu_alert1: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				cpu_crit1: trip1 {
					temperature = <110000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		cpu-thermal2 {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 7>;

			trips {
				cpu_alert2: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				cpu_crit2: trip1 {
					temperature = <110000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		cpu-thermal3 {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 8>;

			trips {
				cpu_alert3: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				cpu_crit3: trip1 {
					temperature = <110000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};

		gpu-thermal {
			polling-delay-passive = <250>;
			polling-delay = <1000>;

			thermal-sensors = <&tsens 9>;

			trips {
				gpu_alert: trip0 {
					temperature = <75000>;
					hysteresis = <2000>;
					type = "passive";
				};
				gpu_crit: trip1 {
					temperature = <95000>;
					hysteresis = <2000>;
					type = "critical";
				};
			};
		};
	};
};
