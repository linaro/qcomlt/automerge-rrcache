// SPDX-License-Identifier: GPL-2.0
/*
 * SDM845 MTP board device tree source
 *
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 */

/dts-v1/;

#include <dt-bindings/regulator/qcom,rpmh-regulator.h>
#include "sdm845.dtsi"

/ {
	model = "Qualcomm Technologies, Inc. SDM845 MTP";
	compatible = "qcom,sdm845-mtp";

	aliases {
		serial0 = &uart9;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};
};

&apps_rsc {
	pm8998-rpmh-regulators {
		compatible = "qcom,pm8998-rpmh-regulators";
		qcom,pmic-id = "a";

		vdd_l7_l12_l14_l15-supply = <&pm8998_s5>;

		smps2 {
			regulator-min-microvolt = <1100000>;
			regulator-max-microvolt = <1100000>;
			qcom,regulator-initial-voltage = <1100000>;
		};

		pm8998_s5: smps5 {
			regulator-min-microvolt = <1904000>;
			regulator-max-microvolt = <2040000>;
			qcom,regulator-initial-voltage = <1904000>;
		};

		pm8998_l1: ldo1 {
			regulator-min-microvolt = <880000>;
			regulator-max-microvolt = <880000>;
			qcom,regulator-initial-voltage = <880000>;
			regulator-initial-mode = <RPMH_REGULATOR_MODE_LPM>;
			qcom,allowed-modes =
				<RPMH_REGULATOR_MODE_LPM
				RPMH_REGULATOR_MODE_HPM>;
			regulator-allow-set-load;
			qcom,mode-threshold-currents = <0 1>;
		};

		pm8998_l2: ldo2 {
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1200000>;
			qcom,regulator-initial-voltage = <1200000>;
			regulator-initial-mode = <RPMH_REGULATOR_MODE_LPM>;
			qcom,allowed-modes =
				<RPMH_REGULATOR_MODE_LPM
				RPMH_REGULATOR_MODE_HPM>;
			regulator-allow-set-load;
			qcom,mode-threshold-currents = <0 30000>;
		};


		ldo7 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			qcom,regulator-initial-voltage = <1800000>;
			qcom,headroom-voltage = <56000>;
			regulator-initial-mode = <RPMH_REGULATOR_MODE_LPM>;
			regulator-allow-set-load;
			qcom,allowed-modes =
				<RPMH_REGULATOR_MODE_LPM
				RPMH_REGULATOR_MODE_HPM>;
			qcom,mode-threshold-currents = <0 10000>;
		};

		pm8998_l20: ldo20 {
			regulator-min-microvolt = <2704000>;
			regulator-max-microvolt = <2960000>;
			qcom,regulator-initial-voltage = <1200000>;
			regulator-initial-mode = <RPMH_REGULATOR_MODE_LPM>;
			qcom,allowed-modes =
				<RPMH_REGULATOR_MODE_LPM
				RPMH_REGULATOR_MODE_HPM>;
			regulator-allow-set-load;
			qcom,mode-threshold-currents = <0 10000>;
		};

		pm8998_l26: ldo26 {
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <1200000>;
			qcom,regulator-initial-voltage = <1200000>;
			regulator-initial-mode = <RPMH_REGULATOR_MODE_LPM>;
			qcom,allowed-modes =
				<RPMH_REGULATOR_MODE_LPM
				RPMH_REGULATOR_MODE_HPM>;
			regulator-allow-set-load;
			qcom,mode-threshold-currents = <0 1>;
		};

		lvs1 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
		};
	};

	pmi8998-rpmh-regulators {
		compatible = "qcom,pmi8998-rpmh-regulators";
		qcom,pmic-id = "b";

		bob {
			regulator-min-microvolt = <3312000>;
			regulator-max-microvolt = <3600000>;
			qcom,regulator-initial-voltage = <3312000>;
		};
	};
};

&i2c10 {
	status = "okay";
	clock-frequency = <400000>;
};

&qupv3_id_1 {
	status = "okay";
};

&uart9 {
	status = "okay";
};

&ufshc {
<<<<<<<
	status = "ok";
=======
	status = "okay";
>>>>>>>

	vcc-supply = <&pm8998_l20>;
	vcc-voltage-level = <2950000 2960000>;
	vcc-max-microamp = <600000>;
	vccq2-max-microamp = <600000>;

	qcom,vddp-ref-clk-supply = <&pm8998_l2>;
	qcom,vddp-ref-clk-max-microamp = <100>;
<<<<<<<

};

&ufsphy {
	status = "ok";
=======
};

&ufsphy {
	status = "okay";
>>>>>>>

	vdda-phy-supply = <&pm8998_l1>; /* 0.88v */
	vdda-pll-supply = <&pm8998_l26>; /* 1.2v */
	vdda-phy-max-microamp = <62900>;
	vdda-pll-max-microamp = <18300>;
};

/* PINCTRL - additions to nodes defined in sdm845.dtsi */

&qup_i2c10_default {
	pinconf {
		pins = "gpio55", "gpio56";
		drive-strength = <2>;
		bias-disable;
	};
};

&qup_uart9_default {
	pinconf-tx {
		pins = "gpio4";
		drive-strength = <2>;
		bias-disable;
	};

	pinconf-rx {
		pins = "gpio5";
		drive-strength = <2>;
		bias-pull-up;
	};
};

&adsp_pil {
	status = "okay";
};

&cdsp_pil {
	status = "okay";
};
